class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.boolean :danger
      t.boolean :injured
      t.boolean :place_to_sleep
      t.boolean :next_meal
      t.boolean :employed

      t.timestamps
    end
  end
end
