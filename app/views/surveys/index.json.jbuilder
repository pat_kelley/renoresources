json.array!(@surveys) do |survey|
  json.extract! survey, :id, :danger, :injured, :place_to_sleep, :next_meal, :employed
  json.url survey_url(survey, format: :json)
end
