json.array!(@resources) do |resource|
  json.extract! resource, :id, :name, :category
  json.url resource_url(resource, format: :json)
end
